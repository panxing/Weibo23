//
//  MainViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/1/30.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    //属性
    let mainTabbar = MainTabbar()
    
    
    //MARK: 按钮监听事件
    //unrecognized selector sent to instance 0x7fdeb2d10040  找不到该方法
    //1.不希望外界访问改方法 可以加上 private关键字 来限制访问
    //2.一旦targe - action 对应的方法添加了private 之后 运行循环就无法找到该方法
    //3.在方法前添加 @objc 告诉运行循环 这个方法是基于OC消息动态派发机制来执行
    @objc private func plusBtnDidClick() {
        printLog(__FUNCTION__)
        let compose = ComposeViewController()
        //包到导航控制器中
        let nav = BaseNavViewController(rootViewController: compose)
        presentViewController(nav, animated: true, completion: nil)
    }
    
    
    //相当于控制器的入口
    override func viewDidLoad() {
        super.viewDidLoad()
        //打印对象的类型
        printLog(tabBar.classForCoder)
        
        //由于系统的tabbar是只读属性 不能够直接设置 可以通过kvc的方式在运行时 替换身份
        setValue(mainTabbar, forKey: "tabBar")
        let actionName = "plusBtnDidClick"
        //直接在方法的参数中传递字符串 会直接将字符串转化为 Selector 类型
        //但是不能够直接传递字符串的引用
        mainTabbar.plusBtn.addTarget(self, action: Selector(actionName), forControlEvents: .TouchUpInside)
        printLog(tabBar.classForCoder)
        //添加子视图控制器
        addChildViewControllers()
    }
    
    //私有函数 只有在本类中才可以访问
    private func addChildViewControllers() {
        addChildViewController(HomeTableViewController(), title: "首页", imageName: "tabbar_home",index: 0)
        addChildViewController(MessageTableViewController(), title: "消息", imageName: "tabbar_message_center", index: 1)
        addChildViewController(DiscoverTableViewController(), title: "发现", imageName: "tabbar_discover",index: 2)
        addChildViewController(ProfileTableViewController(), title: "我", imageName: "tabbar_profile",index: 3)
        
    }
    
    //MARK: 添加单个视图控制器
    private func addChildViewController(vc: UIViewController, title: String,imageName: String, index: Int) {
        //设置渲染颜色
//        tabBar.tintColor = UIColor.orangeColor()
        let nav = BaseNavViewController(rootViewController: vc)
        vc.title = title
        //可以使tabbar的title 和 导航条的title 不同
//        vc.tabBarItem.title  = "首页"
//        vc.navigationItem.title = "哈哈哈"
        
        //设置tabbaritem的图片
        vc.tabBarItem.image = UIImage(named: imageName)
        
        //设置tag值 
        vc.tabBarItem.tag = index
//        vc.tabBarItem.selectedImage = UIImage(named: imageName + "_highlighted")?.imageWithRenderingMode(.AlwaysOriginal)
//        vc.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.orangeColor()], forState: .Selected)
        
        addChildViewController(nav)
    }
    
    /*
    1.要知道用户点击的是 哪一个 item
    2.需要知道点击的是一个 tabbarButton
    3.需要将 tabbarButton中的imageView 取出来  实现动画效果
    */
    
    //tabbar的协议方法  本身就遵守了这个协议 不需要重复的遵守
    override func tabBar(tabBar: UITabBar, didSelectItem item: UITabBarItem) {
//        print(item.tag)
        //能使用image做动画吗  不能够做动画
//        print(item.image)
        
        
        var index = 0
        for v in tabBar.subviews {
            if v.isKindOfClass(NSClassFromString("UITabBarButton")!) {
                
                //如何确定点击的是哪个 tabbarButton
                if item.tag == index {
                    //遍历点击的 tabbarButton  获取其中 imageView
                    for subView in v.subviews {
                        if subView.isKindOfClass(NSClassFromString("UITabBarSwappableImageView")!) {
                            //给imageView添加动画
                             subView.transform = CGAffineTransformMakeScale(0.5, 0.5)
                            UIView.animateWithDuration(0.27, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 2, options: [], animations: { () -> Void in
                                //完成形变的操作
                                subView.transform = CGAffineTransformIdentity
                                }, completion: { (_) -> Void in
                                    print("OK")
                            })
                        }
                    }
                }
                index++
            }
        }
    }

}
