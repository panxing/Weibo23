//
//  MainTabbar.swift
//  SinaWeibo23
//
//  Created by apple on 16/1/30.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class MainTabbar: UITabBar {


    //你对自己代码比较有自信 不要多想 就直接clean
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(plusBtn)
        
    }

    //默认实现报错  通过xib/sb加载该视图 程序就会报错
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //布局子视图的方法
    override func layoutSubviews() {
        super.layoutSubviews()
        //子视图有哪些视图
//        printLog(subviews)
        
        //确定tabbarButton大小
        let w = self.bounds.width / 5
        let h = self.bounds.height
        let rect = CGRect(x: 0, y: 0, width: w, height: h)
        
        //定义一个 能够递增的索引
        var index: CGFloat = 0
        
        //遍历子视图 获取 UITabBarButton
        for subView in subviews {
            //保证 通过UITabBarButton 字符串 可以获取对应的类型
            if subView.isKindOfClass(NSClassFromString("UITabBarButton")!) {
//                printLog(subView)
                //修改frame
                subView.frame = CGRectOffset(rect, w * index, 0)
//                if index == 1 {
//                    //多递增一次
//                    index++
//                }
//                
//                index++
                //三目运算符
                index += (index == 1 ? 2 : 1)
            }
        }
        
        //设置加号按钮的位置
        plusBtn.frame = CGRectOffset(rect, 2 * w, 0)
    }
    
//    lazy var btn: UIButton = UIButton()
    
    lazy var plusBtn: UIButton = UIButton(backImageName: "tabbar_compose_button", imageName: "tabbar_compose_icon_add")
    
//    
//    
//    {
//        let btn = UIButton()
//        //设置背景图片
//        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: .Normal)
//        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: .Highlighted)
//        
//        //设置图片
//        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState: .Normal)
//        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: .Highlighted)
//        self.addSubview(btn)
//        
//        return btn
//    }()

}
