//
//  VisitorLoginView.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/16.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
//需要导入命名空间
import SnapKit

/*
OC: @protocol xxx<NSObject> 
swift protocol xxx: NSObjectprotocol  继承自根协议
*/
//定义协议
@objc protocol VisitorLoginViewDelegate: NSObjectProtocol {
    //声明协议方法 
    //登录的协议方法
    
    //声明可选的协议方法 使用 optional关键字(是OC中的关键字)
    optional func userWillLogin()
    
    //注册的协议方法
    func userWillRegister()
}


class VisitorLoginView: UIView {

    
    //声明代理对象  注意 在声明代理对象一定要使用 weak 防止循环引用
    weak var visitorLoginViewDelegate: VisitorLoginViewDelegate?
    
    
    //自定义访客视图
    //默认的构造方法
    override init(frame: CGRect) {
        //调用父类的构造方法
        super.init(frame: frame)
        setupUI()
    }
    
    
    //一旦重写了 init(frame方法之后 系统会默认该视图会通过手写代码创建
    //系统就不会再让同构xib加载视图
    //通过xib加载的UIView及其子类会调用这个方法
    required init?(coder aDecoder: NSCoder) {
        //系统默认在这个地方执行报错
        fatalError("init(coder:) has not been implemented")
        //super.init(coder: aDecoder)
    }
    
    
    //点击事件的监听方法
    @objc private func registerBtnDidClick() {
        //使用代理调用协议方法
        //OC中调用使用代理调用协议方法  需要做两重判断 1.代理对象不为空  2.代理对象是否响应协议方法
        visitorLoginViewDelegate?.userWillRegister()
    }
    
    @objc private func loginBtnDidClick() {
        //双重判断 1.代理对象不为空  2.代理对象是否响应协议方法
        visitorLoginViewDelegate?.userWillLogin?()
    }
    
    
    //MARK: 设置页面信息
    func setupInfoWtih(tipText: String,imageName: String?) {
        tipLabel.text = tipText
        if imageName != nil {
            //一定不是首页
            circleIcon.image = UIImage(named: imageName!)
            //隐藏大房子
            largeHouse.hidden = true
            //需要将circleIcon 移到最顶层
            self.bringSubviewToFront(circleIcon)
        } else {
            //一定是在首页
            //circleView做动画
            startAnimation()
        }
        
    }
    
    private func startAnimation() {
        let anim = CABasicAnimation(keyPath: "transform.rotation")
        //重复次数
        anim.repeatCount = MAXFLOAT
        anim.toValue = 2 * M_PI
        //设置动画事件
        anim.duration = 20
        //当动画效果完成 / 视图处于非活跃状态下 动画效果不移除图层
        anim.removedOnCompletion = false
        //将基础动画添加到图层
        circleIcon.layer.addAnimation(anim, forKey: nil)
    }
    
    private func setupUI() {
        addSubview(circleIcon)
        addSubview(backImage)
        addSubview(largeHouse)
        addSubview(tipLabel)
        addSubview(loginBtn)
        addSubview(registerBtn)
        
        //手写代码需要手动设置一个属性  屏蔽frame布局的方式
//        circleIcon.translatesAutoresizingMaskIntoConstraints = false
        //设置布局
        //VFL 布局的核心类
        //获取一个布局的约束
        /*
        1.item: 需要添加约束的对象
        2.attribute 需要添加的约束的属性 是一个枚举
        3.relatedBy: 对其方式 一般使用精准约束 Equal
        4.toItem: 相对于哪个视图添加约束
        5. attribute 相对参照的视图的属性
        6.multiplier 乘积系数 一般为 1
        7.constant约束值 (调整值)
        
        
        //返回的约束对象 遵守这个公式
        view1.attr1 = view2.attr2 * multiplier + constant
        */
        //获取一个约束对象
//        self.addConstraint(NSLayoutConstraint(item: circleIcon, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1.0, constant: 0))
//        self.addConstraint(NSLayoutConstraint(item: circleIcon, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1.0, constant: 0))
        
        ///添加约束对象
        circleIcon.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(self.snp_centerX)
            make.centerY.equalTo(self.snp_centerY).offset(-80)
        }
        
        largeHouse.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(circleIcon.snp_center)
        }
        
        tipLabel.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(circleIcon.snp_centerX)
            make.top.equalTo(circleIcon.snp_bottom).offset(16)
            //设置宽度
            make.width.equalTo(230)
            //设置高度
            make.height.equalTo(40)
            
        }
        
        //设置按钮的约束
        loginBtn.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(tipLabel.snp_left)
            make.width.equalTo(100)
            make.top.equalTo(tipLabel.snp_bottom).offset(16)
            
        }
        
        //设置注册按钮的约束
        registerBtn.snp_makeConstraints { (make) -> Void in
            make.right.equalTo(tipLabel.snp_right)
            make.width.equalTo(100)
            make.top.equalTo(tipLabel.snp_bottom).offset(16)
            
        }
        
        //设置背景图片的约束
        backImage.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(circleIcon.snp_center)
        }
        
        let value: CGFloat = 236.0 / 255.0
        //let value: CGFloat = 232.0 / 255.0
        self.backgroundColor = UIColor(red: value, green: value, blue: value, alpha: 1)
//        self.backgroundColor = UIColor(white: 0.93, alpha: 1)
        
        
        //添加点击事件
        loginBtn.addTarget(self, action: "loginBtnDidClick", forControlEvents: .TouchUpInside)
        registerBtn.addTarget(self, action: "registerBtnDidClick", forControlEvents: .TouchUpInside)
        
    }
    
    
    //子视图控件
    //使用懒加载获取
    private lazy var backImage: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_mask_smallicon"))
    private lazy var circleIcon: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_smallicon"))
    private lazy var largeHouse: UIImageView = UIImageView(image: UIImage(named: "visitordiscover_feed_image_house"))
    //提示用户登录的文案
//    private lazy var tipLabel: UILabel = UILabel.factoryLabel("关注一些人,回这里看看有什么惊喜", fontSize: 14, textColor: UIColor.lightGrayColor())
    private lazy var tipLabel: UILabel = UILabel(text: "关注一些人,回这里看看有什么惊喜", fontSize: 14, textColor: UIColor.lightGrayColor())
 
    
    //登录按钮
    private lazy var loginBtn: UIButton = UIButton(backImageName: "common_button_white_disable", title: "登录", textColor: UIColor.darkGrayColor())
    
    
    
//    {
//       let btn = UIButton()
//        //设置按钮
//        btn.setTitle("登录", forState: .Normal)
//        btn.setTitleColor(UIColor.darkGrayColor(), forState: .Normal)
//        let image = UIImage(named: "common_button_white_disable")!
//        let w = Int(image.size.width * 0.5)
//        let h = Int(image.size.height * 0.5)
//        btn.setBackgroundImage(image.stretchableImageWithLeftCapWidth(w, topCapHeight: h), forState: .Normal)
//        return btn
//    }()
    
    //注册按钮
    private lazy var registerBtn: UIButton = UIButton(backImageName: "common_button_white_disable", title: "注册", textColor: themeColor)
    
    
}
