//
//  BaseTableViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/16.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

//在OC中有多继承吗,如果没有,用什么替代呢   -> 协议
class BaseTableViewController: UITableViewController, VisitorLoginViewDelegate{

    
    //用户是否登录的标记'   如果token 不为nil 就表示用户已经登录成功
    var userLogin = AccountViewModel.sharedAccountViewModel.userLogin
    
    //访客视图属性
    var visitorLoginView: VisitorLoginView?
    
    //loadView方法 是苹果专门为手写代码准备的方法  一旦实现了该方法  xib/sb就会失效
    //在loadView方法 没有调用super.loadView方法之前 不能够追踪根视图(view) 会自动检测view是否为nil 如果为 nil  会自动调用 loadView方法
    //准备视图控制器的根视图
    //自定义根视图 就在这个方法中完成
    
    //目标 是根据用户是否登录来显示访客视图还是tableView
    override func loadView() {
        
        if userLogin {
            super.loadView() //加载tableview
        } else {
            //显示自定义的访客视图 引导用户登录或者注册
            visitorLoginView = VisitorLoginView()
            //指定代理
            visitorLoginView?.visitorLoginViewDelegate = self
//            v.backgroundColor = UIColor.purpleColor()
            view = visitorLoginView
            
            
            //设置渲染色 
//            navigationController?.navigationBar.tintColor = UIColor.orangeColor()
            //设置 appearance() 全局样式
            //必须提前设置  在控件被实例化之前就需要设置渲染颜色
//            UINavigationBar.appearance().tintColor = UIColor.orangeColor()
            //设置导航条的按钮
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "登录", style: .Plain, target: self, action: "userWillLogin")
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "注册", style: .Plain, target: self, action: "userWillRegister")
        }
    }
    
    //必选的协议方法 必须实现 如果不实现 就会报错误
    
    func userWillLogin() {
        printLog(__FUNCTION__)
        let oauth = OAuthViewController()
        let nav = UINavigationController(rootViewController: oauth)
        presentViewController(nav, animated: true, completion: nil)
        
    }
    
    func userWillRegister() {
        printLog(__FUNCTION__)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}
