//
//  BaseNavViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/23.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class BaseNavViewController: UINavigationController, UIGestureRecognizerDelegate {
    
    //控制器的入口
    override func viewDidLoad() {
         super.viewDidLoad()
        
        //一旦自定义了返回按钮 原有的返回手势 就会自动失效
        self.interactivePopGestureRecognizer?.delegate = self
    }
    
    override func pushViewController(viewController: UIViewController, animated: Bool) {
        printLog("哈哈哈哈哈")
        
        //获取子视图控制器的数量
        let count = childViewControllers.count
        if count != 0 {
            viewController.hidesBottomBarWhenPushed = true
            
            let btn = UIButton(backImageName: nil, title: "返回", textColor: UIColor.darkGrayColor(), imageName: "navigationbar_back_withtext", fontSize: 14)
            btn.addTarget(self, action: "back", forControlEvents: .TouchUpInside)
            viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
        }

        //BaseNavViewController(rootViewController: vc)  其实也会执行push 的操作
        super.pushViewController(viewController, animated: animated)
        
    }
    
    
    @objc private func back() {
        self.popViewControllerAnimated(true)
    }
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        printLog("哈哈哈哈")
//        if childViewControllers.count > 1 {
//            printLog("嘿嘿嘿嘿")
//            return true
//        }
//        return false
        
        return childViewControllers.count > 1
    }
}
