//
//  StatusCellBottomView.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import SnapKit

class StatusCellBottomView: UIView {

    //MARK: 用户点击事件
    //每一个继承自 UIReponder的对象 都会添加到 响应者链条中
    //就需要遍历响应者链条 获取 导航视图控制器  进行页面跳转
    @objc private func repostBtnDidClick() {
        let temp = TempViewController()
//        temp.hidesBottomBarWhenPushed = true
        navViewController()?.pushViewController(temp, animated: true)
    }
    
    //重写构造方法
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(white: 0.8, alpha: 1)
        setupUI()
    }
    
    //默认实现报错
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //设置界面
    private func setupUI() {
        addSubview(repostBtn)
        addSubview(commentBtn)
        addSubview(oYeahBtn)
        
        //设置约束
        repostBtn.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(self.snp_left)
//            make.centerY.equalTo(self.snp_centerY)
            make.top.equalTo(self.snp_top)
            make.bottom.equalTo(self.snp_bottom)
        }
        
        commentBtn.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(repostBtn.snp_right)
            //            make.centerY.equalTo(self.snp_centerY)
            make.top.equalTo(self.snp_top)
            make.bottom.equalTo(self.snp_bottom)
            make.width.equalTo(repostBtn.snp_width)
        }
        
        oYeahBtn.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(commentBtn.snp_right)
            //            make.centerY.equalTo(self.snp_centerY)
            make.top.equalTo(self.snp_top)
            make.bottom.equalTo(self.snp_bottom)
            
            //三等分的第一步 最右边的视图添加相对于父视图右边的约束
            make.right.equalTo(self.snp_right)
            make.width.equalTo(commentBtn.snp_width)
        }
        
        let sepView1 = sepView()
        let sepView2 = sepView()
        
        addSubview(sepView1)
        addSubview(sepView2)
        //设置约束
        sepView1.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(repostBtn.snp_right)
            make.centerY.equalTo(self.snp_centerY)
            //黄金分割比  1 : 0.618
            make.height.equalTo(self.snp_height).multipliedBy(0.4)
            make.width.equalTo(0.5)
        }
        // view1.attr1 = view2.attr2 * multiplier + constant
        sepView2.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(commentBtn.snp_right)
            make.centerY.equalTo(self.snp_centerY)
            make.height.equalTo(self.snp_height).multipliedBy(0.4)
            make.width.equalTo(0.5)
        }
        
        
        //添加点击事件
        repostBtn.addTarget(self, action: "repostBtnDidClick", forControlEvents: .TouchUpInside)
        
    }
    
    
    private func sepView() -> UIView {
        let v = UIView()
        v.backgroundColor = UIColor.darkGrayColor()
        return v
    }
    
    //懒加载视图
    //转发按钮
    private lazy var repostBtn: UIButton = UIButton(backImageName: nil, title: "转发", textColor: UIColor.lightGrayColor(),imageName: "timeline_icon_retweet")
    //评论按钮
    private lazy var commentBtn: UIButton = UIButton(backImageName: nil, title: "评论", textColor: UIColor.lightGrayColor(),imageName: "timeline_icon_comment")
    //点赞的按钮
    private lazy var oYeahBtn: UIButton = UIButton(backImageName: nil, title: "赞", textColor: UIColor.lightGrayColor(),imageName: "timeline_icon_unlike")
}
