//
//  StatusCellOriginalView.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage


class StatusCellOriginalView: UIView {
   
    //添加约束对象属性 做记录使用
    var bottomConstaint: Constraint?
    
    
    //添加模型属性
    var status: Status? {
        didSet {
            //<a href=\"http://weibo.com/\" rel=\"nofollow\">微博 weibo.com</a>
            //绑定数据
            iconImageView.sd_setImageWithURL(status?.user?.profile_url)
            nameLabel.text = status?.user?.name
            mbrank_image.image = status?.user?.mbrank_image
            //TODO: 后期完善
            timeLabel.text = NSDate.sinaDate(status?.created_at ?? "")?.fullDescription()
//            printLog(status?.created_at)
            sourceLabel.text = status?.sourceText
            printLog(status?.source)
            textLabel.attributedText = EmoticonManager.sharedEmoticoManager.getImageTextWithEmoticonText(status?.text ?? "", font: textLabel.font)
            
            //给配图视图的数组赋值
            pictureView.imageURLs = status?.imageURLs
            //根据数据中是否有配图视图 然后动态调整约束关系  涉及到cell的复用 约束对象也会被复用
            //status?.imageURLs 永远都不为空 如果没有配图视图会给定一个空数组
            //where : 可以获取到前面快速赋值不为空的对象
            //如果imageURLs 不为空 并且 imageURLs.count 不为 0
            
            //解决约束冲突
            //1.先卸载老的约束对象  -> 使用属性 记录底部的约束对象
            self.bottomConstaint?.uninstall()
            //2.根据条件重新添加约束对象
            if let imageURLs = status?.imageURLs where imageURLs.count != 0 {
                //有配图视图
                //原创微博底部相对于配图视图进行约束  同一条约束存在就执行更新 不存在就执行添加约束
                self.snp_updateConstraints(closure: { (make) -> Void in
                     self.bottomConstaint = make.bottom.equalTo(pictureView.snp_bottom).offset(StatusCellMargin).constraint
                })
            } else {
                //没有配图视图
                //原创微博底部相对于微博正文进行约束
                self.snp_updateConstraints(closure: { (make) -> Void in
                    // constraint 获取自动布局的约束对象
                    self.bottomConstaint = make.bottom.equalTo(textLabel.snp_bottom).offset(StatusCellMargin).constraint
                })
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.yellowColor()
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //设置UI
    private func setupUI() {
        //添加子视图
        sepView.backgroundColor = UIColor.redColor()
        addSubview(sepView)
        addSubview(iconImageView)
        addSubview(nameLabel)
        addSubview(mbrank_image)
        addSubview(timeLabel)
        addSubview(sourceLabel)
        addSubview(verified_type_image)
        addSubview(textLabel)
        addSubview(pictureView)
        
        
        sepView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top)
            make.left.equalTo(self.snp_left)
            make.right.equalTo(self.snp_right)
            make.height.equalTo(10)
            
        }
        
        //设置约束
        iconImageView.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(self.snp_left).offset(StatusCellMargin)
            make.top.equalTo(sepView.snp_bottom).offset(StatusCellMargin)
            //约束大小
            make.size.equalTo(CGSize(width: 40, height: 40))
            
        }
        
        nameLabel.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(iconImageView.snp_right).offset(StatusCellMargin)
            make.top.equalTo(iconImageView.snp_top)
            
        }
        mbrank_image.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(nameLabel.snp_right).offset(StatusCellMargin)
            make.top.equalTo(nameLabel.snp_top)
        }
        
        timeLabel.snp_makeConstraints { (make) -> Void in
            make.bottom.equalTo(iconImageView.snp_bottom)
            make.left.equalTo(iconImageView.snp_right).offset(StatusCellMargin)
        }
        
        sourceLabel.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(timeLabel.snp_right).offset(StatusCellMargin)
            make.top.equalTo(timeLabel.snp_top)
        }
        
        verified_type_image.snp_makeConstraints { (make) -> Void in
            //在右下角位置
            make.right.equalTo(iconImageView.snp_right)
            make.bottom.equalTo(iconImageView.snp_bottom)
        }
        
        textLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(iconImageView.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(iconImageView.snp_left)
            make.right.equalTo(self.snp_right).offset(-StatusCellMargin)
        }
        
        pictureView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(textLabel.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(textLabel.snp_left)
            //右边的约束不需要添加  在四张图片显示的时候 配图视图的布局有变化
//            make.right.equalTo(textLabel.snp_right)
            //设置测试大小
            make.size.equalTo(CGSize(width: 100, height: 100))
        }
        
        //给原创微博底部添加约束
        self.snp_makeConstraints { (make) -> Void in
            self.bottomConstaint = make.bottom.equalTo(pictureView.snp_bottom).offset(StatusCellMargin).constraint
        }
        
        //将用户头像处理成圆角
        iconImageView.layer.cornerRadius = 20
        iconImageView.layer.masksToBounds = true
    }
    
    
    private lazy var sepView: UIView = UIView()
    
    //懒加载视图
    private lazy var iconImageView: UIImageView = UIImageView(image: UIImage(named: "avatar_default_big"))
    
    //用户名称 
    private lazy var nameLabel: UILabel = UILabel(text: "23期童鞋们", fontSize: 15, textColor: themeColor)
    
    //微博时间
    private lazy var timeLabel: UILabel = UILabel(text: "22:22", fontSize: 12, textColor: UIColor.lightGrayColor())
    
    //微博来源
    private lazy var sourceLabel: UILabel = UILabel(text: "隔壁老王", fontSize: 12, textColor: UIColor.lightGrayColor())
    
    //微博等级
    private lazy var mbrank_image: UIImageView = UIImageView(image: UIImage(named: "common_icon_membership"))
    
    //用户认证类型
    private lazy var verified_type_image: UIImageView = UIImageView(image: UIImage(named: "avatar_vip"))
    

    
    //微博时间
    private lazy var textLabel: UILabel = UILabel(text: "听说下雨天巧克力和辣条更配哦", fontSize: 14, textColor: UIColor.darkGrayColor(), alignment: NSTextAlignment.Left)
    
    //配图视图
    private lazy var pictureView: StatusPictureView = StatusPictureView()
}
