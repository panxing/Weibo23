//
//  StatusCell.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import SnapKit

class StatusCell: UITableViewCell {

    
    var bottomViewTopConstaint: Constraint?
    
    //添加模型属性
    var status: Status? {
        didSet {
            //设置原创微博的数据模型
            statusOriginalView.status = status
            
            //卸载约束对象
            self.bottomViewTopConstaint?.uninstall()
            //需要根据数据来调整约束关系
            if let re = status?.retweeted_status {
                //转发微博
                
                //给转发微博设置数据模型
                statusRetweetedView.retweetedStatus = re
                statusCellBottomView.snp_makeConstraints { (make) -> Void in
                    //添加约束对象 并且记录约束对象
                    self.bottomViewTopConstaint =  make.top.equalTo(statusRetweetedView.snp_bottom).constraint
                }
                
                //显示转发微博视图  不能够被计算为 CGSizeZero  如果不做隐藏操作 会导致数据显示错乱
                statusRetweetedView.hidden = false
                
            } else {
                //原创微博
                statusCellBottomView.snp_makeConstraints { (make) -> Void in
                    //添加约束对象 并且记录约束对象
                    self.bottomViewTopConstaint =  make.top.equalTo(statusOriginalView.snp_bottom).constraint
                }
                //隐藏转发微博视图
                statusRetweetedView.hidden = true
            }
        }
    }
    
    //xib加载的视图 才会走这个方法
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //cell的默认的构造方法
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        //完成自定义的操作
        setupUI()
        //设置cell 选中样式
        selectionStyle = .None
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupUI() {
        //添加子视图  需要使用 contentView来进行添加
        contentView.addSubview(statusOriginalView)
        contentView.addSubview(statusCellBottomView)
        contentView.addSubview(statusRetweetedView)
        //设置约束
        statusOriginalView.snp_makeConstraints { (make) -> Void in
            make.left.equalTo(contentView.snp_left)
            make.right.equalTo(contentView.snp_right)
            make.top.equalTo(contentView.snp_top)
            //设置高度
//            make.height.equalTo(80)
            
            //遍历约束对象查询 然后进行约束匹配  不是特别高效
//            make.left.right.top.equalTo(contentView)
        }
        
        
        statusRetweetedView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(statusOriginalView.snp_bottom)
            make.left.equalTo(statusOriginalView.snp_left)
            make.right.equalTo(statusOriginalView.snp_right)
        }
        
        statusCellBottomView.snp_makeConstraints { (make) -> Void in
            //添加约束对象 并且记录约束对象
            self.bottomViewTopConstaint =  make.top.equalTo(statusRetweetedView.snp_bottom).constraint
            make.left.equalTo(contentView.snp_left)
            make.right.equalTo(contentView.snp_right)
            //设置高度
            make.height.equalTo(35)
        }
        
        //给contentView添加约束
        contentView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top)
            make.left.equalTo(self.snp_left)
            make.right.equalTo(self.snp_right)
            //最关键的约束
            make.bottom.equalTo(statusCellBottomView.snp_bottom)
        }
    }
    
    
    //懒加载子视图
    private lazy var statusOriginalView: StatusCellOriginalView = StatusCellOriginalView()

    //转发微博部分视图
    private lazy var statusRetweetedView: StatusRetweetedView = StatusRetweetedView()
    
    //底部视图
    private lazy var statusCellBottomView: StatusCellBottomView = StatusCellBottomView()
    
   
    

}
