//
//  StatusRetweetedView.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/22.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import SnapKit
class StatusRetweetedView: UIView {

    
    //添加约束对象属性 做记录使用
    var bottomConstaint: Constraint?
    
    var retweetedStatus: Status? {
        didSet {
            //设置数据
            retweetedLabel.attributedText = EmoticonManager.sharedEmoticoManager.getImageTextWithEmoticonText(retweetedStatus?.text ?? "", font: retweetedLabel.font)
            pictureView.imageURLs = retweetedStatus?.imageURLs
            //卸载老的约束对象
            self.bottomConstaint?.uninstall()
            //根据数据调整约束关系
            if let urls = retweetedStatus?.imageURLs where urls.count != 0 {
                // 转发微博有配图视图
                self.snp_makeConstraints { (make) -> Void in
                    self.bottomConstaint = make.bottom.equalTo(pictureView.snp_bottom).offset(StatusCellMargin).constraint
                }
            } else {
                //转发微博没有配图视图
                self.snp_makeConstraints { (make) -> Void in
                    self.bottomConstaint = make.bottom.equalTo(retweetedLabel.snp_bottom).offset(StatusCellMargin).constraint
                }
            }
        }
    }
    //重写构造方法
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(white: 0.95, alpha: 1)
        setupUI()
    }
    
    //默认实现报错
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //设置界面
    private func setupUI() {
        addSubview(retweetedLabel)
        addSubview(pictureView)
        
        //设置约束
        retweetedLabel.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(self.snp_top).offset(StatusCellMargin)
            make.left.equalTo(self.snp_left).offset(StatusCellMargin)
            make.right.equalTo(self.snp_right).offset(-StatusCellMargin)
        }
        
        pictureView.snp_makeConstraints { (make) -> Void in
            make.top.equalTo(retweetedLabel.snp_bottom).offset(StatusCellMargin)
            make.left.equalTo(retweetedLabel.snp_left)
        }
        
        //转发微博的地步需要添加约束
        self.snp_makeConstraints { (make) -> Void in
           self.bottomConstaint = make.bottom.equalTo(pictureView.snp_bottom).offset(StatusCellMargin).constraint
        }
    }
    
    //懒加载视图
    private lazy var retweetedLabel: UILabel = UILabel(text: "转发微博:终于等到你", fontSize: 14, textColor: UIColor.darkGrayColor(), alignment: .Left)
    
    //配图视图
    private lazy var pictureView: StatusPictureView = StatusPictureView()

}
