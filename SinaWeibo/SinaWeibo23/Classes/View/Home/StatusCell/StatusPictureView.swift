//
//  StatusPictureView.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/22.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage
/*
单图会按照图片等比例显示
多图的图片大小固定
多图如果是4张，会按照 2 * 2 显示
多图其他数量，按照 n * 3 九宫格显示
*/

class StatusPictureView: UICollectionView {

    
    //配置类型的属性
    private let pictureCellId = "pictureCellId"
    
    let itemWidth = (ScreenWidth - StatusCellMargin * 2 - 2 * PictureCellMargin) / 3
    //配图视图的数据源数组
    var imageURLs: [NSURL]? {
        didSet {
            //一旦知道了配图视图的具体的数据之后 
            //1.计算配图视图的大小
            let pSize = calclatePictureViewSize()
            //2.更新配图视图的大小
            self.snp_updateConstraints { (make) -> Void in
                make.size.equalTo(pSize)
            }
            
            //刷新视图  只要tableView 或者 collectionView的数据源发生了更改 就需要调用 reloadData
            reloadData()
            
            //更新测试label的文案
            testLabel.text = "\(imageURLs?.count ?? 0)"
            
        }
    }
    
    
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        //实例化流水布局
        let flowLayout = UICollectionViewFlowLayout()
        //设置layout
        flowLayout.itemSize = CGSizeMake(itemWidth, itemWidth)
        //设置最小间距
        flowLayout.minimumInteritemSpacing = PictureCellMargin
        flowLayout.minimumLineSpacing = PictureCellMargin
        super.init(frame: CGRectZero, collectionViewLayout: flowLayout)
        self.registerClass(PictureCell.self, forCellWithReuseIdentifier: pictureCellId)
        //设置数据源
        self.dataSource = self
        //设置代理
        self.delegate = self
        self.scrollsToTop = false
        //设置背景色
        self.backgroundColor = UIColor.whiteColor()
        
        //添加测试label
        self.addSubview(testLabel)
        testLabel.snp_makeConstraints { (make) -> Void in
            make.center.equalTo(self.snp_center)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //1.根据配图视图的数量 来计算配图视图的真实大小
    private func calclatePictureViewSize() -> CGSize {
        
        //1.获取图片数量
        let imageCount = imageURLs?.count ?? 0
        if imageCount == 0 {
            return CGSizeZero
        }
        
        
        //获取流水布局对象
        let layout = self.collectionViewLayout as! UICollectionViewFlowLayout
        //如果是单张图片
        if imageCount == 1 {
            
            //返回和图片大小一致的size
            //TODO: 后期完善
            //1,需要根据图片的url 获取缓存中缓存的图片对象]
            //从磁盘中加载图片对象
            
            var imageSize = CGSize(width: 100, height: 150)
            let key = imageURLs![0].absoluteString
            let image = SDWebImageManager.sharedManager().imageCache.imageFromDiskCacheForKey(key)
            
            if image != nil {
                imageSize = image.size
            }
            
            //            let imageSize = CGSize(width: 150, height: 200)
            //需要设置itmeSize 和图片大小一致
            
            //设置itemSize
            layout.itemSize = imageSize
            
            
            return imageSize
        }
        
        //如果不是单张图片 需要重新设置collectiView 的cell的大小  防止cell复用时候出现显示的问题
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        
        if imageCount == 4 {
            //配图视图大小的计算
            let w = itemWidth * 2 + PictureCellMargin
            return CGSize(width: w, height: w)
        }
        
        //程序走到这个地方 就是其他多张图片显示的情况
        /*
        1,2,3   -> 1
        4,5,6   -> 2
        7,8,9   -> 3
        */
        //1.计算行数
        let row = CGFloat((imageCount - 1) / 3 + 1)
        let w = itemWidth * 3 + PictureCellMargin * 2
        let h = itemWidth * row + (row - 1) * PictureCellMargin
        return CGSizeMake(w, h)
    }
    
    private lazy var testLabel: UILabel = UILabel(text: "", fontSize: 40, textColor: UIColor.redColor())

}


extension StatusPictureView: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageURLs?.count ?? 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(pictureCellId, forIndexPath: indexPath) as! PictureCell
        
        //设置调试颜色 
        //indexPath.item % 2 == 0  判断奇数 偶数
//        cell.backgroundColor = indexPath.item % 2 == 0 ? UIColor.redColor() : UIColor.greenColor()
        cell.imageURL = imageURLs![indexPath.item]
        return cell
    }
    
    
    //点击cell的协议方法
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let browser = SDPhotoBrowser()
        browser.sourceImagesContainerView = self
        browser.imageCount = imageURLs?.count ?? 0
        browser.currentImageIndex = indexPath.item
        browser.delegate = self
        browser.show()
        
        /*
        SDPhotoBrowser *browser = [[SDPhotoBrowser alloc] init];
        
        browser.sourceImagesContainerView = 原图的父控件;
        
        browser.imageCount = 原图的数量;
        
        browser.currentImageIndex = 当前需要展示图片的index;
        
        browser.delegate = 代理;
        
        [browser show]; // 展示图片浏览器
        
        */
    }
}


extension StatusPictureView: SDPhotoBrowserDelegate {
    
    // 返回高质量图片的url  最初点击的图片是缩量图 给定对应的缩略图的高质量的url
    func photoBrowser(browser: SDPhotoBrowser!, highQualityImageURLForIndex index: Int) -> NSURL! {
        //1.获取当前url对应的 urlString
        if let url = imageURLs?[index] {
            //2.将urlString 中的 thumbnail 替换为 bmiddle
            let urlString = url.absoluteString
            let str = urlString.stringByReplacingOccurrencesOfString("thumbnail", withString: "bmiddle")
           // 3.将urlString 转换为 NSURL 对象
            return NSURL(string: str)
        }
        return nil
    }
    
    
    //// 返回临时占位图片（即原来的小图）
    func photoBrowser(browser: SDPhotoBrowser!, placeholderImageForIndex index: Int) -> UIImage! {
        //返回cell 的image 对象
        //1.获取indexPath
        let indexPath = NSIndexPath(forItem: index, inSection: 0)
        //2.获取用户点击的cell
        if let cell = self.cellForItemAtIndexPath(indexPath) as? PictureCell {
            return cell.iconView.image
        }
        
        return nil
    }
}



//类之间关系很密切 并且没有太多复杂的逻辑 是可以写在同一个文件中
private class PictureCell: UICollectionViewCell {
    
    //外部设置的图片url属性
    var imageURL: NSURL? {
        didSet {
            iconView.sd_setImageWithURL(imageURL)
        }
    }
    
    
    //重写构造方法
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor(white: 0.95, alpha: 1)
        setupUI()
    }
    
    //默认实现报错
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //设置界面
    private func setupUI() {
        contentView.addSubview(iconView)
        //设置约束
        iconView.snp_makeConstraints { (make) -> Void in
            make.edges.equalTo(contentView.snp_edges)
        }
    }
    
    //懒加载视图
    private lazy var iconView: UIImageView = {
        let iv = UIImageView()
        //UIImageView默认的显示方式  ScaleToFill: 缩放全部视图进行填充
        //显得很山寨
//        iv.contentMode = .ScaleToFill
        // Aspect: 部分  缩放部分 进行填充  视图的比例不会失真 但是 图片会被裁剪
        iv.contentMode = .ScaleAspectFill
        //缩放图片  部分图片进行适应的显示  会导致 imageView可能会留白  一般也不推荐使用这样方式来显示图片
//        iv.contentMode = .ScaleAspectFit
        //手动设置裁剪
        iv.clipsToBounds = true
        return iv
    }()
}
