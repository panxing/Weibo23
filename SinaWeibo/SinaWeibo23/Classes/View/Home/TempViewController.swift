//
//  TempViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/23.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class TempViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //手码创建的视图控制器和视图 背景色 是 UIColor.clearColor
        self.view.backgroundColor = UIColor.whiteColor()
        
        //一旦完成的了导航条的返回按钮的自定义 就需要自己添加点击事件
//        let btn = UIButton(backImageName: nil, title: "返回", textColor: UIColor.darkGrayColor(), imageName: "navigationbar_back_withtext", fontSize: 14)
//        btn.addTarget(self, action: "back", forControlEvents: .TouchUpInside)
//        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btn)
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "push", style: .Plain, target: self, action: "push")
    }
    
//    @objc private func back() {
//        navigationController?.popViewControllerAnimated(true)
//    }
    
    @objc private func push() {
        let temp = TempViewController()
        //导航视图的控制器在子页面中的放回按钮的文案 是上一个视图控制器的title,如果上一个视图控制器没有title 会给定一个默认的 '返回"
        navigationController?.pushViewController(temp, animated: true)
    }
}
