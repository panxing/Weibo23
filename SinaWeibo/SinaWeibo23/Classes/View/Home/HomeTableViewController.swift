//
//  HomeTableViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/1/30.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import AFNetworking
import SVProgressHUD

class HomeTableViewController: BaseTableViewController {
    
    private var lastCount = 0
    //模型数组
    private lazy var statusListViewModel = StatusListViewModel()
    //cellID
    private let homeCellId = "homeCellId"
    //下拉刷新控件
    let myRefrshControl = WBRefreshControl()
    
    //准备视图层次 此时 根视图还是为nil  在没有super.loadView之前 不要追踪view
    //检测到根视图为 nil 会执行loadView
    //为手写代码准备的 一旦实现这个方法  xib/sb就会失效
    override func loadView() {
        super.loadView()
    }
    
    //视图层次准备完毕之后调用这个方法   一般需要自己添加视图 就在这个方法中执行
    //只会执行一次
    override func viewDidLoad() {
        super.viewDidLoad()
        printLog(view)
        if !userLogin {
            //用户没有登录 当前的根视图是访客视图 就不要设置tableView
            //设置访客视图的信息
            visitorLoginView?.setupInfoWtih("关注一些人，回这里看看有什么惊喜", imageName: nil)
            return
        }
        
        prepareForTableView()
//        //加载数据
        loadData()
        
        //设置提醒的小icon
        // '定时' 刷新接口数据 根据后端接口数据 修改 值  只能显示字符串
        tabBarItem.badgeValue = "0"
        
    }
    
    private func prepareForTableView() {
        //注册cell 
        //fatal error: unexpectedly found nil while unwrapping an Optional value
        //用户没有登录的情况下 根视图是 访客视图 
        tableView.registerClass(StatusCell.self, forCellReuseIdentifier: homeCellId)
        //设置cell的高度
        //要使用tableView能够自动计算行高需要设置两个属性
        //1.设置预估行高  尽量要接近真实的行高
        tableView.estimatedRowHeight = 300
        //2.设置行高为自动计算 (自动布局系统来计算行高) +  3.tableView的cell的contentView添加约束
        tableView.rowHeight = UITableViewAutomaticDimension
        
        //设置分割线样式
        tableView.separatorStyle = .None
        
//        //系统下拉刷新控件
//        refreshControl = UIRefreshControl()
//        //添加触发事件
//        refreshControl?.addTarget(self, action: "loadData", forControlEvents: .ValueChanged)
        
        //添加自定义下拉刷新控件
        self.tableView.addSubview(myRefrshControl)
        //添加监听事件
        myRefrshControl.addTarget(self, action: "loadData", forControlEvents: .ValueChanged)
        //将小菊花设置 为 tableFooterView
        tableView.tableFooterView = indicatorView
        //小菊花开始转动  才会显示
//        indicatorView.startAnimating()
        
        //设置点击状态栏快速回到顶部
        tableView.scrollsToTop = true
        
        
        
        //添加提示的文案
        self.navigationController?.navigationBar.addSubview(tipLabel)
        //移动最下面
        self.navigationController?.navigationBar.insertSubview(tipLabel, atIndex: 0)
    }
    
    
    // viewDidLoad 和viewDidAppear有啥区别 在使用的时候需要注意哪些
    //每次页面显示的时候 都会执行
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
//        loadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
         super.viewDidDisappear(animated)
    }

    //MARK: 加载首页数据
    @objc private func loadData() {
        //使用视图模型来获取 网络请求的结果  -> 模型数组
        statusListViewModel.loadHomeData(indicatorView.isAnimating()) { (isSuccess) -> () in
            //等待网络请求结束之后 修改控件的刷新状态
            self.myRefrshControl.endRefreshing()
            if isSuccess {
                if self.indicatorView.isAnimating() {
                    //上拉操作
                    //停止转动
                    self.indicatorView.stopAnimating()
                } else {
                    //下拉刷新的操作
                    //刷新成功的提示
                    self.showTipLabel()
                }
                //记录上一次数组中模型的数量
                self.lastCount = self.statusListViewModel.statuses.count
                //请求数据成功
                self.tableView.reloadData()
                
            } else {
                //请求数据失败
                SVProgressHUD.showErrorWithStatus("网络君正在睡觉,请稍后再试")
            }
        }
    }
    
    
    private func showTipLabel() {
        //记录数组中上一次的数据的数量
        let delta = self.statusListViewModel.statuses.count - lastCount
        let tipText = delta == 0 ? "没有新微博" : "有\(delta)条新微博"
        tipLabel.text = tipText
        
        let rect = tipLabel.frame
        //执行动画
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.tipLabel.frame = CGRect(x: 0, y: 44, width: ScreenWidth, height: 44)
            }) { (_) -> Void in
//                print("OK")
                UIView.animateKeyframesWithDuration(0.25, delay: 1, options: [], animations: { () -> Void in
                    //恢复初始位置
                    self.tipLabel.frame = rect
                    }, completion: { (_) -> Void in
                        print("OK")
                })
        }
        
    }
    private lazy var indicatorView: UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .Gray)

    //自定义提示视图
    private lazy var tipLabel: UILabel = {
        let l = UILabel(frame: CGRect(x: 0, y: -64, width: ScreenWidth, height: 44))
        l.textColor = UIColor.whiteColor()
        l.textAlignment = .Center
        l.backgroundColor = themeColor
        return l
    }()
}

//MARK: tableView的数据源方法
extension HomeTableViewController {
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusListViewModel.statuses.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //使用这个方法 获取cell 手写代码的时候需要手动注册cell
        let cell = tableView.dequeueReusableCellWithIdentifier(homeCellId, forIndexPath: indexPath) as! StatusCell
        // textLabel 是一个懒加载的视图
//        cell.textLabel?.text = statuses[indexPath.row].user?.name
        
        //如果正在加载最后一条cell  并且 小菊花没有转动的情况下 才去获取更多数据
        if indexPath.row == statusListViewModel.statuses.count - 1 && !indicatorView.isAnimating(){
            //正在加载最后一条cell
            //加载数据
            //转动小菊花 等网络数据获取完毕之后再停止转动 才能够继续加载更多数据
            indicatorView.startAnimating()
            //先转动菊花 再加载数据
            loadData()
        }
        cell.status = statusListViewModel.statuses[indexPath.row]
        return cell
    }
}
