//
//  WelcomeViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

import SnapKit
import SDWebImage

class WelcomeViewController: UIViewController {

    override func loadView() {
        //自定义根视图
        view = UIImageView(image: UIImage(named: "ad_background"))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    //根视图在viewWillLayoutsubViews生命周期方法中 它的frame 会被重新设置
    //动画效果一般不要添加在ViewDidLoad中
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        //执行动画效果
        startAnimation()
        
    }
    
    //设置页面信息
    private func setupUI() {
        view.addSubview(iconView)
        view.addSubview(welcomeLabel)
        
        //设置子视图的约束
        iconView.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(view.snp_centerX)
            //自动布局的时候 一般需要写相对的约束
            make.bottom.equalTo(view.snp_bottom).offset(-160)
            //约束头像的大小 是像素的一半
            make.size.equalTo(CGSizeMake(90, 90))
        }
        welcomeLabel.snp_makeConstraints { (make) -> Void in
            make.centerX.equalTo(iconView.snp_centerX)
            make.top.equalTo(iconView.snp_bottom).offset(16)
        }
        //最初文案是隐藏的 等到用户头像动画结束时  在显示欢迎文案
        welcomeLabel.alpha = 0
        
        
        //设置用户头像
        iconView.sd_setImageWithURL(AccountViewModel.sharedAccountViewModel.headImageURL)
        
        //设置用户头像圆角
        iconView.layer.cornerRadius = 45
        //裁剪视图
        iconView.layer.masksToBounds = true
        
    }
    
    
    //头像的动画效果
    private func startAnimation() {
        //使用闭包动画来完成  iOS7.0之后推出的  有弹簧效果
        //1.usingSpringWithDamping  弹簧系数 0 ~ 1 之间 系数越小 弹簧越明显
        //2.initialSpringVelocity  加速度  一般设置 usingSpringWithDamping * 10 ~=  initialSpringVelocity 9.8是重力的加速度
        //3.options 动画的可选项 一般设置  在OC中有多个选项 使用位移枚举 在swift中使用数据来表示   '[]' 表示空数组
        
        // 做约束的修改
        //思考应该移动到顶部的哪个位置  --> 移动到对称位置
        //负值变的更大才合理
        let offset = -UIScreen.mainScreen().bounds.height + 160
        //修改约束  意味底部有两条约束
        //snp_updateConstraints 更新一条相同的约束  
        //当前阶段只是 '收集' 了约束的变化 并没有重新布局页面
        self.iconView.snp_updateConstraints(closure: { (make) -> Void in
            make.bottom.equalTo(self.view.snp_bottom).offset(offset)
        })
        
        UIView.animateWithDuration(1.2, delay: 0, usingSpringWithDamping: 0.98, initialSpringVelocity: 9.8, options: [], animations: { () -> Void in
            //设置强制更新页面 提前更新页面  这一段代码必须写到动画闭包中 才能够产生动画效果
            self.view.layoutIfNeeded()
            }) { (_) -> Void in
                //显示欢迎文案
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.welcomeLabel.alpha = 1
                    }, completion: { (_) -> Void in
                        printLog("OK")
                        //欢迎文案显示结束之后 切换根视图控制器
                        //不推荐这种写法
//                        UIApplication.sharedApplication().keyWindow?.rootViewController = MainViewController()
                        NSNotificationCenter.defaultCenter().postNotificationName(KChoseRootVC, object: "welcome")
                        
                        //通知响应的方法执行完毕之后 才会继续向下执行
                })
        }
    }
    
    //懒加载子视图
    
    //1.用户头像
    private lazy var iconView: UIImageView = UIImageView(image: UIImage(named: "avatar_default_big"))
    
    //2.欢迎的文案
    private lazy var welcomeLabel: UILabel = UILabel(text: (AccountViewModel.sharedAccountViewModel.userName ?? "") + "欢迎归来", fontSize: 18, textColor: UIColor.darkGrayColor())
    


}
