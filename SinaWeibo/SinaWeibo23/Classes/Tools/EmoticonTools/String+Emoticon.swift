//
//  String+Emoticon.swift
//  EmoticonKeyboard
//
//  Created by apple on 16/2/28.
//  Copyright © 2016年 apple. All rights reserved.
//

import Foundation


extension String {
    //将十六进制的字符串转换为 Emoji表情
    func emojiStr() -> String {
        
        //1.将字符串中十六进制的字符串查找出来  获取扫描对象
        let scanner = NSScanner(string: self)
        //将扫描的结果存储在这个地址下
        var value: UInt32 = 0
        scanner.scanHexInt(&value)
        //将十六进制数字转换为 Unicode字符 转换emoji 表情
        let result = Character(UnicodeScalar(value))
        //Emoji表情是Unicode 字
        //😈 emoji 表情
        return "\(result)"
    }
}