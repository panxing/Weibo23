//
//  Emoticon.swift
//  EmoticonKeyboard
//
//  Created by apple on 16/2/28.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class Emoticon: NSObject {
    //分组表情的文件夹的名称
    var id: String?
    
    //发布微博的时候 对应需要发送的图片对应文本
    var chs: String?
    
    //用户输入表情的时候 本地显示的图片
    var png: String? {
        didSet {
            //png 被设置值的时候 就给 imagePath赋值 只会计算一次
            if let groupId = id,imageName = png {
                imagePath = NSBundle.mainBundle().bundlePath + "/Emoticons.bundle/" + "\(groupId)/" + imageName
            }
            
        }
    }
    
    //Emoji表情的对应的十六进制的字符串
    var code: String? {
        didSet {
            if let codeStr = code {
                //给 emojiStr 赋值
                emojiStr = codeStr.emojiStr()
            }
        }
    }
    
    
    var emojiStr: String?
    
    //bundlePath + Emoticons.bundle + id + 图片名称
    var imagePath: String? //{  //每次都会运   }
    
    var isEmpty = false

    var isDelete = false
    init(id: String, dict: [String : String]) {
        self.id = id
        super.init()
        self.setValuesForKeysWithDictionary(dict)
    }
    
    //删除按钮的模型
    init(isDelete: Bool) {
        super.init()
        self.isDelete = isDelete
    }
    
    init(isEmpty: Bool) {
        self.isEmpty = isEmpty
        super.init()
    }
    
    //过滤
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {   }
    
    
    //重写description属性
    override var description: String {
        let keys = ["chs","png","code"]
        let dict = self.dictionaryWithValuesForKeys(keys)
        return dict.description
    }
}

