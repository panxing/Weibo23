//
//  EmoticonAttachment.swift
//  EmoticonKeyboard
//
//  Created by apple on 16/2/29.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class EmoticonAttachment: NSTextAttachment {

    //表情图片对应的文本
    var chs: String?
    
    
    //将表情图片转换成属性文本
    class func emoticonImageToImageText(em: Emoticon, font: UIFont) -> NSAttributedString {
        //NSTextAttachment '附件'   类型
        let attachment = EmoticonAttachment()
        //1.将附件类型添加图片
        attachment.image = UIImage(contentsOfFile: em.imagePath ?? "")
        attachment.chs = em.chs
        //1.1设置附件对象的bounds
        let lineHeight = font.lineHeight
        attachment.bounds = CGRect(x: 0, y: -4, width: lineHeight, height: lineHeight)
        //属性字符串  富文本  可以通过附件来实例化属性文本  一般不使用 不可变的属性字符串
        let str = NSAttributedString(attachment: attachment)
        //转换成可变的字符串  第一次输入时候会跟随前前面的文本的样式 其本身并没有属性 所有的属性还是默认值
        let imageText = NSMutableAttributedString(attributedString: str)
        //1.2需要给文本添加属性  只有可变的属性文本才能添加属性
        imageText.addAttribute(NSFontAttributeName, value: font, range: NSRange(location: 0, length: 1))
        
        return imageText
    }
}
