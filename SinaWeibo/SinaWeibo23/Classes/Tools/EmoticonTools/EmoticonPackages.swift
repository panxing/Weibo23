//
//  EmoticonPackages.swift
//  EmoticonKeyboard
//
//  Created by apple on 16/2/28.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

/*

每隔20个按钮需要一个删除按钮 --> 就是添加删除按钮的数据源
页面表情不足21个需要补足空表情
*/

class EmoticonPackages: NSObject {
    //中文名称
    var group_name_cn: String?
    //分组表情的字典类型数组
    var emoticons: [[String : String]]?
    
    var emoticonList = [Emoticon]()
    
    init(id: String, name: String, array: [[String : String]]) {
        
        super.init()
        group_name_cn = name
        emoticons = array
        //遍历数组
        
        var index = 0
        for item in array {
            let e = Emoticon(id: id, dict: item)
            //添加模型
            emoticonList.append(e)
            index++
            
            //判断index == 20
            if index == 20 {
                //添加删除的模型
                let delete = Emoticon(isDelete: true)
                //将模型添加到数组中
                emoticonList.append(delete)
                //复位 
                index = 0
            }
        }
        
        // 添加完删除按钮之后 需要判断 页面表情不足21个需要补足空表情
        self.addEmptyEmoticon()
    }
    
    
    private func addEmptyEmoticon() {
        let detla = emoticonList.count % 21
        
        if detla == 0 {
            //不需要补足空表情
            return
        }
        
        for _ in detla..<20 {
            let e = Emoticon(isEmpty: true)
            emoticonList.append(e)
        }
        //最后一个添加删除表情
        let delete = Emoticon(isDelete: true)
        emoticonList.append(delete)
    }
}
