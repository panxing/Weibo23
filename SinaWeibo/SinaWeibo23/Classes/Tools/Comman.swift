//
//  Comman.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/16.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

//和OC中的 pch 文件类似

//命名空间的作用 可以全局访问
let themeColor = UIColor.orangeColor()


//App第三方授权的信息
let client_id = "1947360907"
let redirect_uri = "http://www.itcast.cn"
let client_secret = "4c4e4d8250fc0cce13108d61e50c9e40"



//在什么情况下使用通知呢,通知的关联性比较差 一般不推荐大量使用 
/*
1.页面的视图之间有联系 但是视图层次嵌套非常深的时候  使用通知可以简化消息传递的过程(解耦)
2.页面之间没有联系  一般使用通知来完成消息的传递
3.一对多的情况下 一般也是使用通知来解决
*/
//定义通知的对应的字符串
let KChoseRootVC = "KChoseRootVC"



//微博布局的间距
let StatusCellMargin: CGFloat = 10
//配图视图cell之间的间距  也是选择图片的间距
let PictureCellMargin: CGFloat = 5


//屏幕尺寸信息
let ScreenWidth = UIScreen.mainScreen().bounds.width
let ScreenHeight = UIScreen.mainScreen().bounds.height



/// 输出日志
///
/// - parameter message:  日志消息
/// - parameter logError: 错误标记，默认是 false，如果是 true，发布时仍然会输出
/// - parameter file:     文件名
/// - parameter method:   方法名
/// - parameter line:     代码行数
func printLog<T>(message: T,
    logError: Bool = false,
    file: String = __FILE__,
    method: String = __FUNCTION__,
    line: Int = __LINE__)
{
    if logError {
        print("\((file as NSString).lastPathComponent)[\(line)], \(method): \(message)")
    } else {
        #if DEBUG
            print("\((file as NSString).lastPathComponent)[\(line)], \(method): \(message)")
        #endif
    }
}
