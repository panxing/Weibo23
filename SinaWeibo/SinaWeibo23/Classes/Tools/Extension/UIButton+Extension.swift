//
//  UIButton+Extension.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/16.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

extension UIButton {
    
    //给 UIButton 扩展一个 构造函数 
    //1.背景图片 + 图片
    //注意: 如果按钮设置背景图片 按钮就会自动有大小
    convenience init(backImageName: String?,imageName: String?) {
        self.init()
        if backImageName != nil {
            setBackgroundImage(UIImage(named: backImageName!), forState: .Normal)
            setBackgroundImage(UIImage(named: backImageName! + "_highlighted"), forState: .Highlighted)
        }
        
        if imageName != nil {
            setImage(UIImage(named: imageName!), forState: .Normal)
            
            //设置高亮
            setImage(UIImage(named: imageName! + "_highlighted"), forState: .Highlighted)
        }
        
        
        //自适应大小
        sizeToFit()
    }
    
    //2.背景图片 + 文案
    convenience init(backImageName: String?,title: String?,textColor: UIColor,imageName: String? = nil,fontSize: CGFloat = 14) {
        self.init()
        if backImageName != nil {
            //按钮的大小会根据北京图片自动修改
            setBackgroundImage(UIImage(named: backImageName!), forState: .Normal)
        }
        if imageName != nil {
            setImage(UIImage(named: imageName!), forState: .Normal)
        }
        
        //设置字体的大小
        titleLabel?.font = UIFont.systemFontOfSize(fontSize)
        setTitle(title, forState: .Normal)
        setTitleColor(textColor, forState: .Normal)
        //自适应大小
        sizeToFit()
    }
}
