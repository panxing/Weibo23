//
//  UIView+ViewController.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/23.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

extension UIView {
    //遍历视图的响应者链条 获取导航视图控制器

    func navViewController() -> UINavigationController? {
        //遍历视图的响应者链条
        //获取视图的下一个响应者
        var next = nextResponder()
        while next != nil {
            if let nextObj = next as? UINavigationController {
                return nextObj
            }
            //获取下一个响应的这 下一个响应者
            next = next?.nextResponder()
            printLog(next)
        }
        return nil
    }
}
