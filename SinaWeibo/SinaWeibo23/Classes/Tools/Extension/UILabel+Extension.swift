//
//  UILabel+Extension.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/16.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

//给UILabel做扩展
extension UILabel {
    
    //类工厂方法
    class func factoryLabel(text: String, fontSize: CGFloat,textColor: UIColor) -> UILabel{
        let l = UILabel()
        l.text = text
        l.font = UIFont.systemFontOfSize(fontSize)
        l.textColor = textColor
        //设置行数为 0
        l.numberOfLines = 0
        //设置对其
        l.textAlignment = .Center
        //自适应大小
        l.sizeToFit()
        
        return l
    }
    
    //扩展一个便利的构造方法
    //在扩展中只能够添加便利的构造函数 不能够添加指定的构造函数
    //1.遍历的构造函数中只能够用 self.init形式调用指定指定的构造函数
    //2.不能够重写,也不能使用 super
    //3.可以被继承
    //4.必须依赖于  '指定' 的构造函数来获取一个对象
    
    //函数的参数如果给定默认值 可以不传该参数 
    convenience init(text: String, fontSize: CGFloat,textColor: UIColor, alignment: NSTextAlignment = .Center) {
        //获取到一个UILabel类型的对象
        self.init()
        
        self.text = text
        font = UIFont.systemFontOfSize(fontSize)
        self.textColor = textColor
        //设置行数为 0
        numberOfLines = 0
        //设置对其
        textAlignment = alignment
        //自适应大小
        sizeToFit()
    }
    
}
