//
//  NSDate+Extension.swift
//  1.时间转换
//
//  Created by apple on 16/2/29.
//  Copyright © 2016年 apple. All rights reserved.
//

import Foundation

extension NSDate {
    
    class func sinaDate(dateString: String) -> NSDate? {
        let df = NSDateFormatter()
        //执行日期格式
        df.dateFormat = "EEE MMM dd HH:mm:ss zzz yyyy"
        
        //在真机上如果没有指定本地化信息 日期将无法转换
        df.locale = NSLocale(localeIdentifier: "en")
        //获取字符对应的日期对象
        //自动设置为标准事件
        let date = df.dateFromString(dateString)
        return date
    }
    
    func fullDescription() -> String {
        let df = NSDateFormatter()
        df.dateFormat = "MM-dd HH:mm"
        //获取对应的字符串对象
        let fullDes = df.stringFromDate(self)
        
        return fullDes
    }
}