//
//  UIColor+RandomColor.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/25.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

extension UIColor {
    
    //随机色
    class func randomColor() -> UIColor {
        //确定 r g b 随机值
        let r = CGFloat(Double(random() % 256) / 255.0)
        let g = CGFloat(Double(random() % 256) / 255.0)
        let b = CGFloat(Double(random() % 256) / 255.0)
        return UIColor(red: r, green: g, blue: b, alpha: 1)
    }
}
