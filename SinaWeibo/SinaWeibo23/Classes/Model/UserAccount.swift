//
//  UserAccount.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class UserAccount: NSObject, NSCoding {
    //用于调用access_token，接口获取授权后的access token。
    var access_token: String?
    
    //access_token的生命周期，单位是秒数。token生成时 过多少秒之后会过期
    var expires_in: NSTimeInterval = 0 {
        didSet {
            expires_date = NSDate(timeIntervalSinceNow: expires_in)
        }
    }
    
    //token过期的日期 开发者账号的过期日期是 5年 普通用户的账户过期日期是3天
    var expires_date: NSDate?
    
    //当前授权用户的UID。  标识一个唯一的用户
    var uid: String?
    
    //友好显示名称
    var name: String?
    
    //用户头像地址（大图），180×180像素
    var avatar_large: String?
    
    //使用KVC设置部分属性初始值
    init(dict: [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    //过滤操作
    override func setValue(value: AnyObject?, forUndefinedKey key: String) { }
    
    //重写description属性
    override var description: String {
        let keys = ["access_token","expires_in","uid","name","avatar_large","expires_date"]
        let dict = self.dictionaryWithValuesForKeys(keys)
        return dict.description
    }
    
    
    
    //MARK: 存储模型数据
    func saveAccount() {
        //通常将数据保存在沙盒中
        //文件路径拼接的方法 在Xcode7.0被搞丢了
        let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("useraccount.plist")
        printLog(path)
        //调用归档的方法 
        NSKeyedArchiver.archiveRootObject(self, toFile: path)
    }
    
    
    //MARK: 读取模型数据
    class func loadUserAccount() -> UserAccount? {
        let path = (NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last! as NSString).stringByAppendingPathComponent("useraccount.plist")
        if let userAccount = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? UserAccount {
            //需要判断用户的token是否过期
            //只有过期日期大于当前日期 才没有过期
            if userAccount.expires_date?.compare(NSDate()) == NSComparisonResult.OrderedDescending {
                return userAccount
            }
        }
        
        return nil
    }
    
    
    //实现协议方法
    //解归档  将二进制数据 转换为自定义对象 -> 反序列化比较像
    required init?(coder aDecoder: NSCoder) {
        //给当前的对象的属性赋值
        access_token = aDecoder.decodeObjectForKey("access_token") as? String
        //基本数据类型在解归档的时候可以得到明确类型 不需要做类型转换
        expires_in = aDecoder.decodeDoubleForKey("expires_in")
        name = aDecoder.decodeObjectForKey("name") as? String
        avatar_large = aDecoder.decodeObjectForKey("avatar_large") as? String
        uid = aDecoder.decodeObjectForKey("uid") as? String
        expires_date = aDecoder.decodeObjectForKey("expires_date") as? NSDate
    }
    
    //归档   将自定义对象 转换为二进制数据存储在沙盒中(磁盘)  -> 和序列化比较像
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(access_token, forKey: "access_token")
        aCoder.encodeDouble(expires_in, forKey: "expires_in")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
        aCoder.encodeObject(uid, forKey: "uid")
        aCoder.encodeObject(expires_date, forKey: "expires_date")
    }
    
}
