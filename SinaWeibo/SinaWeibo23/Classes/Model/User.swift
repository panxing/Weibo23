//
//  User.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class User: NSObject {
    //用户头像地址（中图），50×50像素
    var profile_image_url: String?
    
    //计算用户头像的url对象
    var profile_url: NSURL? {
        return NSURL(string: profile_image_url ?? "")
    }
    
    //友好显示名称
    var name: String?
    
    /// 认证类型，-1：没有认证，0，认证用户，2,3,5: 企业认证，220: 达人 草根
    var verified_type: Int = -1
    
    var verified_type_image: UIImage? {
        switch verified_type {
        case -1: return nil
        case 0: return UIImage(named: "avatar_vip")
        case 2,3,5: return UIImage(named: "avatar_enterprise_vip")
        case 220: return UIImage(named: "avatar_grassroot")
        default: return nil
        }
    }
    
    ///// 会员等级 0-6
    var mbrank: Int = 0
    
    var mbrank_image: UIImage? {
        if mbrank > 0 && mbrank < 7 {
            return UIImage(named: "common_icon_membership_level\(mbrank)")
        }
        return UIImage(named: "common_icon_membership")
    }
    
    //使用KVC设置初始值
    init(dict: [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    
    //过滤
    override func setValue(value: AnyObject?, forUndefinedKey key: String) { }
}
