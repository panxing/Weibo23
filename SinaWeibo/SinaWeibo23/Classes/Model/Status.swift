//
//  Status.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/19.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

class Status: NSObject {
    
    //	微博ID  Int 和OC中 NSInteger是一样  永远匹配当前机型的位数
    //iPhone 4 ~ iPhone 5c 都是32位的机型  在这些机型上面 对应的微博id 会被截取
    //iPhone 5s ~ 最新的机型 都是64的机型
    var id: Int64 = 0
    //微博创建时间
    var created_at: String?
    
    //微博正文
    var text: String?
    
    //微博来源
    var source: String? {
        didSet {
            //<a href=\"http://weibo.com/\" rel=\"nofollow\">微博 weibo.com</a>
            // Range<Index> 泛型对象
            //如何声明swift中的rang对象
//            let range = 0..<10
            let startRange = source?.rangeOfString("\">")
            let endRange = source?.rangeOfString("</a")
            if let range1 = startRange, range2 = endRange {
                let range = range1.endIndex..<range2.startIndex
                //            let ocrange = NSRange(location: 10, length: 10)
                sourceText = source?.substringWithRange(range)
            }
        }
    }
    
    
    var sourceText: String?
    
    //用户模型属性
    var user: User?
    
    //转发微博模型  嵌套在 微博字段中
    //如果是原创微博 retweeted_status 不存在
    var retweeted_status: Status?
    
    //配图视图数组的属性  数组  永远都不可能为nil  服务器在没有配图的情况下 依然会返回一个空数组 而非 null对象
    var pic_urls: [[String : String]]? {
        didSet {
            guard let urls = pic_urls else {
                // pic_urls 就直接return  在set方法 中不能够return值
                return
            }
            
            //如果urls不为空 需要实例化 NSURL类型的图片数组
            imageURLs = [NSURL]()
            //遍历 urls  去对应的图片地址(String)
            for item in urls {
                let urlString = item["thumbnail_pic"]
//                let str =  urlString?.stringByReplacingOccurrencesOfString("thumbnail", withString: "bmiddle")
                // 第一个'!'相信后端一定能够给一个urlString
                // 第二个 '!' 相信后端给的urlString 一定是一个合法的urlString
                let url = NSURL(string: urlString!)!
                //将url对象添加到数组中
                imageURLs?.append(url)
            }
        }
    }
    
    //将 pic_urls内的字典遍历 并且取出对应的值 转换为 NSURL类型 -> 添加到这个属性数组中
    var imageURLs: [NSURL]?
    
    
    //计算在下载单张图片的时候该下载谁的图片
    //被转发的原创微博中如果有图片  转发微博中就一定不会有图片
    var pictureURLs: [NSURL]? {
        if retweeted_status != nil {
            //转发微博
            return retweeted_status?.imageURLs
        }
        return imageURLs
    }
    //使用KVC设置初始值
    init(dict: [String : AnyObject]) {
        super.init()
        //遍历键值 给对象转发 setValue: forKey: 消息
        setValuesForKeysWithDictionary(dict)
    }
    
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "user" {
            //给user属性初始化
            //将AnyObject转换为 字典对象
            if let dict = value as? [String : AnyObject] {
                // 对 user做字典转模型的操作
                user = User(dict: dict)
                //需要提前终止 否则字典转模型 就白做了
                return
            }
        }
        
        if key == "retweeted_status" {
            //给user属性初始化
            //将AnyObject转换为 字典对象
            if let dict = value as? [String : AnyObject] {
                // 对 user做字典转模型的操作
                retweeted_status = Status(dict: dict)
                //需要提前终止 否则字典转模型 就白做了
                return
            }
        }
        
        //很重要的一步  调用super
        super.setValue(value, forKey: key)
    }
    //过滤
    override func setValue(value: AnyObject?, forUndefinedKey key: String) { }
}
