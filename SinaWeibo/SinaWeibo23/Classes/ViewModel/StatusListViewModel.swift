//
//  StatusListViewModel.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/23.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import AFNetworking
import SDWebImage
import SVProgressHUD

class StatusListViewModel: NSObject {
    
    
    //模型数组
    lazy var statuses = [Status]()
    
    
    func loadHomeData(isPullUp: Bool, finished: (isSuccess: Bool) -> ()) {
        let urlString = "2/statuses/home_timeline.json"
        guard let token = AccountViewModel.sharedAccountViewModel.access_token else {
            SVProgressHUD.showErrorWithStatus("请登录")
            return
        }
        var parameters = ["access_token": token]
        //下拉刷新数据 减少流量的消耗  pageCount  index
        //"start"
        var since_id: Int64 = 0
        //上拉加载更多数据 避免一次加载用户的所有数据
        var max_id: Int64 = 0
        if isPullUp {
            //小菊花正在转动  上拉加载更多数据
            max_id = self.statuses.last?.id ?? 0
        } else {
            since_id = self.statuses.first?.id ?? 0
        }
        
        if since_id > 0 {
            parameters["since_id"] = "\(since_id)"
        }
        if max_id > 0 {
            // max_id -1  解决数据重复的bug
            parameters["max_id"] = "\(max_id - 1)"
        }
        
        
        HTTPClient.sharedClient.requestJSONDict(.GET, urlString: urlString, parameters: parameters) { (dict, error) -> () in
            if error != nil {
                finished(isSuccess: false)
                return
            }
            
            //2.在字典中通过  "statuses"这个key 来去获取微博数组数据
            guard let array = dict!["statuses"] as? [[String : AnyObject]] else {
                finished(isSuccess: false)
                return
            }
            //实例化模型数据
            var tempArray = [Status]()
            //3.遍历数组 获取单个的字典对象
            for item in array {
                //4.字典转模型
                let s = Status(dict: item)
                //5.将模型数据添加到数组中
                tempArray.append(s)
            }
            
            //调试服务器在应对 since_id 和 max_id同时传递的情况下 如何处理
//            printLog(tempArray)
            //记录临时数组
            if since_id > 0 {
                //在数组的前面进行添加的操作的操作
                self.statuses = tempArray + self.statuses
            } else if max_id > 0 {
                //上拉加载更多数据  追加数组数据
                //移除第一条数据
//                tempArray.removeFirst()
                self.statuses += tempArray
                //停止小菊花转动  不然只能获取一次更多数据
                //                self.indicatorView.stopAnimating()
            } else {
                //首次加载数据 下拉刷新数据
                self.statuses = tempArray
            }
            //将单张图片下载到本地 之后 才能够回调
//            self.cacheStatusImage(tempArray)
            self.cacheStatusImage(tempArray, finished: finished)
            //执行成功的回调
//            finished(isSuccess: true)
        }
    }
    
    
    //完成单张图片的下载
    private func cacheStatusImage(array: [Status],finished: (isSuccess: Bool) -> ()) {
        if array.count == 0 {
            //获取的数据为空 不需要下载单张图片
            
            
            //需要完成回调  不然在没有加载到新数据  对应刷新状态无法被修改
            finished(isSuccess: true)
            return
        }
        
        
        // '群组队列'监听一组异步组任务完成得到统一回调
        //面临的问题 是多次的异步任务执行 需要得到统一的完成回调
        //遍历模型数组
        let group = dispatch_group_create()
        for s in array {
            //判断模型中 imageURLs数量
            let imageCount = s.pictureURLs?.count ?? 0
            if imageCount == 1 {
//                printLog(s.pictureURLs![0])
                //完成单张图片下载
                //在将要开始异步任务前面 向群组中添加异步任务 任务池的任务 + 1
                // dispatch_group_enter 和 dispatch_group_leave 是成对出现的
                dispatch_group_enter(group)
                SDWebImageManager.sharedManager().downloadImageWithURL(s.pictureURLs![0], options: [], progress: nil, completed: { (_, _, _, _, _) -> Void in
                    //异步任务执行完毕之后 需要在任务池 -1
                    dispatch_group_leave(group)
                    print("完成单张图片的下载")
                })
            }
        }
        
        
        dispatch_group_notify(group, dispatch_get_main_queue()) { () -> Void in
            //需要完成回调 统一的回调
            printLog("原创微博的单张图片下载完成")
            finished(isSuccess: true)
        }
        
        
    }
    
}
