//
//  AccountViewModel.swift
//  SinaWeibo23
//
//  Created by apple on 16/2/17.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit
import AFNetworking


//视图模型 必须包含一个模型
class AccountViewModel: NSObject {
    
    static let sharedAccountViewModel: AccountViewModel = AccountViewModel()
    //定义用户账户模型属性
    var userAccount: UserAccount?
    
    //将构造函数私有化 外界不能直接访问
    private override init() {
        super.init()
        userAccount = UserAccount.loadUserAccount()
        
    }
    
    //现在需要基于模型做相关的逻辑的操作
    
    //用户是否登录
    var userLogin: Bool {
        //计算型属性  只读属性
        return userAccount?.access_token != nil
    }
    
    //计算当前登录用户的token
    var access_token: String? {
        return userAccount?.access_token
    }
    
    //用户名称的只读属性
    var userName: String? {
        return userAccount?.name
    }
    
    //用户头像的url
    var headImageURL: NSURL? {
        if let urlString =  userAccount?.avatar_large  {
            return NSURL(string: urlString)
        }
        return nil
        
    }


    //加载用户的token
    func loadAccessToken(code:  String,finished: (isSuccess: Bool) -> ()) {
        // 使用AFN发起post的http请求
        let urlString = "oauth2/access_token"
        //以字典的格式传递参数
        let parameters = ["client_id":client_id,"client_secret": client_secret ,"grant_type":"authorization_code","code":code, "redirect_uri":redirect_uri]
        let AFN = AFHTTPSessionManager()
        AFN.responseSerializer.acceptableContentTypes?.insert("text/plain")
        
        HTTPClient.sharedClient.requestJSONDict(.POST, urlString: urlString, parameters: parameters) { (dict, error) -> () in
            if error != nil {
                //执行失败的回调
                finished(isSuccess: false)
                return
            }
            //程序走到这里 就一定转换为了字典类型数据
            let userAccount = UserAccount(dict: dict!)
            printLog(userAccount)
            
            //获取用户信息 发起网络请求
            self.loadUserInfo(userAccount, finished: finished)
            
        }

    }
    
    
    //MARK: 请求用户信息
    private func loadUserInfo(userAccount: UserAccount,finished: (isSuccess: Bool) -> ()) {
        //获取用户信息的接口 返回的数据 是AFN默认支持解析的格式 并不是每一个都需要设置解析格式
        let urlString = "2/users/show.json"
        //OC中向字典中添加空对象 只能够添加 [NSNull null]
        //程序员保证一定能够获取到这些属性对应的值
        let parameters = ["access_token": userAccount.access_token!, "uid": userAccount.uid!]
        
        HTTPClient.sharedClient.requestJSONDict(.GET, urlString: urlString, parameters: parameters) { (dict, error) -> () in
            if error != nil {
                //执行失败的回调
                finished(isSuccess: false)
                return
            }
            
            let name = dict!["name"] as! String
            let avatar_large = dict!["avatar_large"] as! String
            //完善模型数据
            userAccount.name = name
            userAccount.avatar_large = avatar_large
            //            printLog(userAccount)
            
            //使用完整的对象 调用归档的方法
            userAccount.saveAccount()
            //用户信息保存完毕之后 执行成功的回调
            
            //需要给单例对象的 用户模型 赋值
            self.userAccount = userAccount
            
            finished(isSuccess: true)
            printLog("come here viewmodel")
            
        }
    }
}
