//
//  AppDelegate.swift
//  SinaWeibo23
//
//  Created by apple on 16/1/30.
//  Copyright © 2016年 apple. All rights reserved.
//

import UIKit

import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        printLog(AccountViewModel.sharedAccountViewModel.userAccount)
        printLog(NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).last)
        setThemeColor()
        //注册通知
        registerNotification()
        AFNetworkActivityIndicatorManager.sharedManager().enabled = true
        //实例化window
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.backgroundColor = UIColor.whiteColor()
        //可视化
        window?.makeKeyAndVisible()
        
        //设置根视图控制器
        window?.rootViewController = defaultRootViewController()
//        let compose = PictureSelectorViewController()
//        let nav = BaseNavViewController(rootViewController: compose)
//        window?.rootViewController = nav
        return true
    }
    
    
    //消息机制才能够找到该方法
    @objc private func AppRootViewController(n: NSNotification) {
        printLog("come here appdelegate")
        window?.rootViewController = n.object == nil ? WelcomeViewController() : MainViewController()
    }
    
    //注册通知
    private func registerNotification() {
        // object 观测哪一个对象发出的通知  如果为 nil 默认是所有对象
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "AppRootViewController:", name: KChoseRootVC, object: nil)
    }
    private func defaultRootViewController() -> UIViewController {
        return AccountViewModel.sharedAccountViewModel.userLogin ? WelcomeViewController() : MainViewController()
    }
    
    private func setThemeColor() {
        //control + i 格式化代码  一经设置全局有效,  必须在控件实例化之前设置
        //必须提前设置  在控件被实例化之前就需要设置渲染颜色
        UINavigationBar.appearance().tintColor = themeColor
        UITabBar.appearance().tintColor = themeColor
    }
    
    //移除通知  编码规范而已
    //代理 对象是一个单例对象
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}

